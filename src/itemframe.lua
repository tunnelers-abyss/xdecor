-- Item frames.

-- Hint:
-- If your item appears behind or too far in front of the item frame, add
--     _xdecor_itemframe_offset = <number>
-- to your item definition to fix it.

local itemframe, tmp = {}, {}
local S = minetest.get_translator("xdecor")
screwdriver = screwdriver or {}

local function remove_item(pos, node)
	local objs = minetest.get_objects_inside_radius(pos, 0.5)
	if not objs then return end

	for _, obj in pairs(objs) do
		local ent = obj:get_luaentity()
		if obj and ent and ent.name == "xdecor:f_item" then
			obj:remove() break
		end
	end
end

local facedir = {
	[0] = {x = 0,  y = 0, z = 1},
	      {x = 1,  y = 0, z = 0},
	      {x = 0,  y = 0, z = -1},
	      {x = -1, y = 0, z = 0}
}

local function update_item(pos, node)
	remove_item(pos, node)
	local meta = minetest.get_meta(pos)
	local itemstring = meta:get_string("item")
	local posad = facedir[node.param2]
	if not posad or itemstring == "" then return end

	local itemdef = ItemStack(itemstring):get_definition()
	local offset_plus = 0
	if itemdef and itemdef._xdecor_itemframe_offset then
		offset_plus = itemdef._xdecor_itemframe_offset
		offset_plus = math.min(6, math.max(-6, offset_plus))
	end
	local offset = (6.5+offset_plus)/16

	pos = vector.add(pos, vector.multiply(posad, offset))
	tmp.nodename = node.name
	tmp.texture = ItemStack(itemstring):get_name()

	local entity = minetest.add_entity(pos, "xdecor:f_item")
	local yaw = (math.pi * 2) - node.param2 * (math.pi / 2)
	entity:set_yaw(yaw)

	local timer = minetest.get_node_timer(pos)
	timer:start(15.0)
end

local function drop_item(pos, node)
	local meta = minetest.get_meta(pos)
	local item = meta:get_string("item")
	if item == "" then return end

	minetest.add_item(pos, item)
	meta:set_string("item", "")
	remove_item(pos, node)

	local timer = minetest.get_node_timer(pos)
	timer:stop()
end

-- New fucntion
function itemframe.destruct(pos)
	local meta = minetest.get_meta(pos)
	local node = minetest.get_node(pos)
	if meta:get_string("item") ~= "" then
		drop_item(pos, node)
	end
end

function itemframe.set_infotext(meta)
	local itemstring = meta:get_string("item")
	local owner = meta:get_string("owner")
	if itemstring == "" then
		if owner ~= "" then
			meta:set_string("infotext", S("@1 (owned by @2)", S("Item Frame"), owner))
		else
			meta:set_string("infotext", S("Item Frame"))
		end
	else
		local itemstack = ItemStack(itemstring)
		local tooltip = itemstack:get_short_description()
		if tooltip == "" then
			tooltip = itemstack:get_name()
		end
		if itemstring == "" then
			tooltip = S("Item Frame")
		end
		if owner ~= "" then
			meta:set_string("infotext", S("@1 (owned by @2)", tooltip, owner))
		else
			meta:set_string("infotext", tooltip)
		end
	end
end

-- New fucntion
function itemframe.destruct(pos)
	local meta = minetest.get_meta(pos)
	local node = minetest.get_node(pos)
	if meta:get_string("item") ~= "" then
		drop_item(pos, node)
	end
end

function itemframe.after_place(pos, placer, itemstack)
	local meta = minetest.get_meta(pos)
	local name = placer:get_player_name()
	meta:set_string("owner", name)
	itemframe.set_infotext(meta)
end

function itemframe.timer(pos)
	local node = minetest.get_node(pos)
	local meta = minetest.get_meta(pos)
	local num = #minetest.get_objects_inside_radius(pos, 0.5)

	if num == 0 and meta:get_string("item") ~= "" then
		update_item(pos, node)
	end

	return true
end

function itemframe.rightclick(pos, node, clicker, itemstack)
	local meta = minetest.get_meta(pos)
	local player_name = clicker:get_player_name()
	if minetest.is_protected(pos, player_name) then return end
	local owner = meta:get_string("owner")
	local admin = minetest.check_player_privs(player_name, "protection_bypass")

	if not admin and not itemstring == "" then --and (player_name ~= owner or not itemstack) then
		return itemstack
	end

	drop_item(pos, node)
	local description = itemstack:get_description() or ""
	local itemname = itemstack:get_name()
	local itemstring = itemstack:take_item():to_string()
	meta:set_string("item", itemstring)
	itemframe.set_infotext(meta)
	update_item(pos, node)

	if itemstring == "" then
	   	if minetest.is_protected(pos, player_name) then return end 
	   	meta:set_string("infotext", "Item Frame (owned by " .. player_name .. ")")
	   	meta:set_string("owner", player_name)
	else
		meta:set_string("infotext", description.." ("..itemname.." owned by " .. owner .. ")")
	end
	
	return itemstack
end

function itemframe.punch(pos, node, puncher)
	local special_items = {"draconis:egg_fire_dragon_black", "draconis:egg_fire_dragon_bronze", "draconis:egg_fire_dragon_green", "draconis:egg_fire_dragon_red", "draconis:egg_ice_dragon_light_blue", "draconis:egg_ice_dragon_sapphire", "draconis:egg_ice_dragon_slate", "draconis:egg_ice_dragon_white"} -- prospector won't work.
	local meta = minetest.get_meta(pos)
	local player_name = puncher:get_player_name()
	local inventory = puncher:get_inventory()
	local owner = meta:get_string("owner")
	local admin = minetest.check_player_privs(player_name, "protection_bypass")
	local wielded = puncher:get_wielded_item():get_name()
	
	for _,v in pairs(special_items) do
		if wielded == v then
			drop_item(pos, node)
			meta:set_string("item", wielded)
			update_item(pos, node)
			inventory:remove_item('main', wielded) -- O_O
			break
		else
			if admin or player_name == owner then
				drop_item(pos, node)
			end
		end
	end
end

function itemframe.dig(pos, player)
	if not player then return end
	local meta = minetest.get_meta(pos)
	local player_name = player and player:get_player_name()
	local owner = meta:get_string("owner")
	local admin = minetest.check_player_privs(player_name, "protection_bypass")

	return admin or player_name == owner
end

function itemframe.blast(pos)
	return
end

xdecor.register("itemframe", {
	description = S("Item Frame"),
	_tt_help = S("For presenting a single item"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	is_ground_content = false,
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_green1", {
	description = "Green item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_green1.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_green1.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_green2", {
	description = "Dark green item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_green2.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_green2.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})


xdecor.register("itemframe_red", {
	description = "Red item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_red.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_red.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_radioactive", {
	description = "Radioactive item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_radioactive.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_radioactive.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_yellow", {
	description = "Yellow item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_yellow.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_yellow.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_pink", {
	description = "Pink item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_pink.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_pink.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_purple", {
	description = "Purple item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_purple.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_purple.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_cyan", {
	description = "Cyan item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_cyan.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_cyan.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})


xdecor.register("itemframe_blue", {
	description = "Blue item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_blue.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_blue.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})

xdecor.register("itemframe_grey", {
	description = "Grey item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_grey.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_grey.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})


xdecor.register("itemframe_orange", {
	description = "Orange item frame",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults(),
	on_rotate = screwdriver.disallow,
	sunlight_propagates = true,
	inventory_image = "xdecor_itemframe_orange.png",
	node_box = xdecor.nodebox.slab_z(0.9375),
	tiles = {
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
		"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_orange.png"
	},
	after_place_node = itemframe.after_place,
	on_timer = itemframe.timer,
	on_rightclick = itemframe.rightclick,
	on_punch = itemframe.punch,
	can_dig = itemframe.dig,
	on_destruct = itemframe.destruct
})


local table = {"green1", "green2", "red", "yellow", "pink", "purple", "cyan", "blue", "grey", "orange"}
for i=1,10,1 do
	xdecor.register("itemframe_"..table[i].. "_radioactive", {
		description = table[i].. " radioactive item frame",
		groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
		sounds = default.node_sound_wood_defaults(),
		on_rotate = screwdriver.disallow,
		sunlight_propagates = true,
		inventory_image = "xdecor_itemframe_"..table[i].."_radioactive.png",
		node_box = xdecor.nodebox.slab_z(0.9375),
		tiles = {
			"xdecor_wood.png", "xdecor_wood.png", "xdecor_wood.png",
			"xdecor_wood.png", "xdecor_wood.png", "xdecor_itemframe_"..table[i].."_radioactive.png"
		},
		after_place_node = itemframe.after_place,
		on_timer = itemframe.timer,
		on_rightclick = itemframe.rightclick,
		on_punch = itemframe.punch,
		can_dig = itemframe.dig,
		on_destruct = itemframe.destruct
	})
end


minetest.register_entity("xdecor:f_item", {
	initial_properties = {
		visual = "wielditem",
		visual_size = {x = 0.33, y = 0.33},
		collisionbox = {0,0,0,0,0,0},
		pointable = false,
		physical = false,
		textures = {"air"},
	},
	on_activate = function(self, staticdata)
		local pos = self.object:get_pos()
		if minetest.get_node(pos).name ~= "xdecor:itemframe" and minetest.get_node(pos).name ~= "xdecor:itemframe_green1" and minetest.get_node(pos).name ~= "xdecor:itemframe_green2" and minetest.get_node(pos).name ~= "xdecor:itemframe_red" and minetest.get_node(pos).name ~= "xdecor:itemframe_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_yellow" and minetest.get_node(pos).name ~= "xdecor:itemframe_pink" and minetest.get_node(pos).name ~= "xdecor:itemframe_purple" and minetest.get_node(pos).name ~= "xdecor:itemframe_cyan" and minetest.get_node(pos).name ~= "xdecor:itemframe_blue" and minetest.get_node(pos).name ~= "xdecor:itemframe_grey" and minetest.get_node(pos).name ~= "xdecor:itemframe_orange" and minetest.get_node(pos).name ~= "xdecor:itemframe_green1_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_green2_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_red_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_yellow_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_pink_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_purple_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_cyan_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_blue_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_grey_radioactive" and minetest.get_node(pos).name ~= "xdecor:itemframe_orange_radioactive"  then
			self.object:remove()
		end

		if tmp.nodename and tmp.texture then
			self.nodename = tmp.nodename
			tmp.nodename = nil
			self.texture = tmp.texture
			tmp.texture = nil
		elseif staticdata and staticdata ~= "" then
			local data = staticdata:split(";")
			if data and data[1] and data[2] then
				self.nodename = data[1]
				self.texture = data[2]
			end
		end
		if self.texture then
			self.object:set_properties({
				textures = {self.texture}
			})
		end
	end,
	get_staticdata = function(self)
		if self.nodename and self.texture then
			return self.nodename .. ";" .. self.texture
		end

		return ""
	end
})

-- Recipes

minetest.register_craft({
	output = "xdecor:itemframe",
	recipe = {
		{"group:stick", "group:stick", "group:stick"},
		{"group:stick", "default:paper", "group:stick"},
		{"group:stick", "group:stick", "group:stick"}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe",
	recipe = {
		{"group:stick", "group:stick", "group:stick"},
		{"group:stick", "default:paper", "group:stick"},
		{"group:stick", "group:stick", "group:stick"}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_green1",
	recipe = {
		{"xdecor:itemframe", "dye:dark_green", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_green2",
	recipe = {
		{"xdecor:itemframe", "dye:green", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_red",
	recipe = {
		{"xdecor:itemframe", "dye:red", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_radioactive",
	recipe = {
		{"xdecor:itemframe", "group:uranium", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_yellow",
	recipe = {
		{"xdecor:itemframe", "dye:yellow", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_pink",
	recipe = {
		{"xdecor:itemframe", "dye:pink", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_purple",
	recipe = {
		{"xdecor:itemframe", "dye:violet", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_cyan",
	recipe = {
		{"xdecor:itemframe", "dye:cyan", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_blue",
	recipe = {
		{"xdecor:itemframe", "dye:blue", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_grey",
	recipe = {
		{"xdecor:itemframe", "dye:grey", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_orange",
	recipe = {
		{"xdecor:itemframe", "dye:orange", ""},
		{"", "", ""},
		{"", "", ""}
	}
})


local tablep = {"red", "yellow", "pink", "cyan", "blue", "grey", "orange"}
for x=1,7,1 do
	minetest.register_craft({
		output = "xdecor:itemframe_"..tablep[x].."_radioactive",
		recipe = {
			{"xdecor:itemframe_radioactive", "dye:"..tablep[x], ""},
			{"", "", ""},
			{"", "", ""}
		}
	})
end

minetest.register_craft({
	output = "xdecor:itemframe_green1_radioactive",
	recipe = {
		{"xdecor:itemframe_radioactive", "dye:dark_green", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_green2_radioactive",
	recipe = {
		{"xdecor:itemframe_radioactive", "dye:green", ""},
		{"", "", ""},
		{"", "", ""}
	}
})

minetest.register_craft({
	output = "xdecor:itemframe_purple_radioactive",
	recipe = {
		{"xdecor:itemframe_radioactive", "dye:violet", ""},
		{"", "", ""},
		{"", "", ""}
	}
})
--[[
minetest.register_lbm({
	label = "Update itemframe infotexts",
	name = "xdecor:update_itemframe_infotexts",
	nodenames = {"xdecor:itemframe"},
	run_at_every_load = true,
	action = function(pos, node)
		local meta = minetest.get_meta(pos)
		itemframe.set_infotext(meta)
	end,
})
]]
